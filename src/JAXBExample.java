import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class JAXBExample {

	public static void main(String[] args) {

		  User user = new User();
		  user.setId(1);
		  user.setName("SUSHMA");
		  user.setAge(21);

		  try {

			File file = new File("C:\\Users\\nimmani.s\\output.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(User.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(user, file);
			jaxbMarshaller.marshal(user, System.out);

		      } catch (JAXBException e) {
			e.printStackTrace();
		      }

		}

}
